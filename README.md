## 项目简介

若依框架修改器是一个可以一键修改RuoYi框架包名、项目名等的工具。是我学习Python时候顺手做的一个小玩意，代码写的不好，共享出来和大家一起学习！有好的建议可以提，欢迎PR！(已支持Mac运行及打包)

* Python 3.7.9（3.9及以上不支持win7，请注意）

* WxPython 4.1.0（4.1.1及以上不支持win7，请注意，win10可能也有问题）

## 开发说明
1. 安装依赖：`$ pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple`
2. 使用vscode开发，打开run.py文件，点击运行→启动调试，或者F5启动程序
3. 其他工具开发，执行`$ python run.py`运行程序
4. Windows打包exe时候直接运行bin/windows/package.bat即可，执行完会生成dist目录，里面是打包好的运行文件（需要安装依赖`$ pip install pyinstaller -i https://pypi.tuna.tsinghua.edu.cn/simple`）
5. Mac打包app时候直接运行bin/mac/package.sh即可，执行完会生成dist目录，里面是打包好的运行文件（需要安装依赖`$ pip install py2app -i https://pypi.tuna.tsinghua.edu.cn/simple`）
6. fbp下的RuoYi-WxPython-UI.fbp文件是页面设计，需要用[wxFormBuilder](https://github.com/wxFormBuilder/wxFormBuilder)打开
7. 如果需要扩展，直接在`core`包下加修改类即可，继承`BaseCore`


## 内置功能

1.  一键修改：支持修改[RuoYi](https://gitee.com/y_project/RuoYi)、[RuoYi-Vue](https://gitee.com/y_project/RuoYi-Vue)、[RuoYi-fast](https://gitee.com/y_project/RuoYi-fast)、[RuoYi-Cloud](https://gitee.com/y_project/RuoYi-Cloud)四个系列的所有版本及其他扩展项目的包名、项目名、配置、其他关键字等
2.  参数配置：配置代码中用到的mysql数据库连接、redis连接等，可以选择启用还是不启用
3.  模板配置：配置修改器默认修改的内容，属于高级功能，不会配置的请不要随意改动
4.  druid秘钥生成：工具可以一键生成druid连接的秘钥。
5.  批量删除文件：删除项目编译后生成的例如target、.settings、.classpath、.project、.idea、.iml、._开头、.DS_Store、__MACOSX、node_modules等文件或文件夹
6.  关于我们：介绍本工具的信息。
7.  打赏作者：弹出打赏的对话框，可以扫码打赏。
8.  检测更新：爬取gitee地址解析标签来判断是否有新版本。

## 修改内容
1. 项目的包名，例如`com.ruoyi`修改为`com.xxx`
2. 项目的项目名，例如ruoyi修改为xxx，包括模块文件夹的名字也会改掉
3. 项目的pom文件以及其中的配置属性
4. 项目的站点名称（或者叫标题），主要体现在页面上
5. 项目的配置文件，比如数据库连接、redis连接等（需要开启配置）
6. 脚本文件的修改，比如bin/xxx.bat或者ry.sh

## 使用说明

1. 当操作记录中出现错误日志，只要不是系统级异常都可以忽略，因为并不是项目修改失败了，而是有的文件修改出现问题，并不影响你的使用，提示的修改错误的文件可以手动去修改下

## 下载体验

- 下载链接：https://gitee.com/lpf_project/RuoYi-MT/releases  
  
## 演示效果

<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0930/102948_7bfce8cc_389553.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0930/103031_1f9f2d37_389553.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0930/103110_41171fca_389553.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0930/103308_c8cfb299_389553.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0518/180150_9d3bbc64_389553.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0518/180216_dad1f1e3_389553.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0518/180238_b3899581_389553.png"/></td>
    </tr>
	<tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0518/180259_9723e951_389553.png"/></td>
    </tr>	
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0519/104945_7244a041_389553.png"/></td>
    </tr>
</table>




## 页面设计

<table>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0519/104057_e34e5dfa_389553.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0519/104153_fb23bfe5_389553.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0519/104234_f238fcc9_389553.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0519/104313_aff1ef57_389553.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0519/104339_ba3a1a4b_389553.png"/></td>
    </tr>
    <tr>
        <td><img src="https://images.gitee.com/uploads/images/2021/0524/173315_09abc49c_389553.png"/></td>
    </tr>
</table>