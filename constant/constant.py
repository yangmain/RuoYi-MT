# -*- coding: utf-8 -*-
# @Time : 2021/05/18
# @Author : ricky
# @File : constant.py
# @Software: vscode
"""
系统常量
"""
APP_VERSION_INT = 420230425
APP_VERSION_STRING = '4.7.7.20230425'
PYTHON_VERSION_STRING = '3.7.9'
WXPYTHON_VERSION_STRING = '4.1.0'
# 禁止修改的目录
FORBID_ALTER_DIR_TUPLE = ('target', 'node_modules')
