#!/bin/sh

echo

function check_module
{
  if python3 -c "import $1" >/dev/null 2>&1
  then
      echo "1"
  else
      echo "0"
  fi
}

v=`python3 -V 2>&1 | grep "Python"`
if [ ! "$v" ]; then
 echo "\033[0;31m检测到当前环境未安装Python3.x，请安装Python3.x后重新运行脚本\033[0m"
 echo
 exit 1
fi

res=`check_module py2app`
if [ $res == 0 ]; then
  echo "\033[0;31m检测到当前环境未安装py2app，请执行'pip3 install py2app'安装py2app后重新运行脚本\033[0m"
  echo
  exit 1
fi

echo
echo "\033[32m[信息] 打包Python项目，生成Mac平台可执行程序\033[0m"
echo

current_dir=$(cd $(dirname $0); pwd)

if [[ -a $current_dir'/build' ]]; then
	rm -rf $current_dir'/build'
fi

if [[ -a $current_dir'/dist' ]]; then
	rm -rf $current_dir'/dist'
fi

python3 setup.py py2app

echo
echo "\033[32m[信息] 打包完成。\033[0m"
echo
