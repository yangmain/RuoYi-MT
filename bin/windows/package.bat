@echo off
echo.
echo [信息] 打包Python项目，生成exe文件。
echo.

%~d0
cd %~dp0

if exist build rd /s /q build

if exist dist rd /s /q dist

if exist *.spec del /q *.spec

call pyinstaller --add-data="../../img;img" --add-data="../../libs;libs" --add-data="../../conf;conf" --version-file version.txt -F -w -n 若依框架修改器 -i icon.ico  ../../run.py 

echo.
echo [信息] 打包完成。
echo.

pause