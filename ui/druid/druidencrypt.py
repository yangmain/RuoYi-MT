# -*- coding: utf-8 -*-
# @Time : 2021/05/06
# @Author : ricky
# @File : druidencrypt.py
# @Software: vscode
"""druid秘钥生成界面
这里是执行jar命令生成的
"""
import wx
import subprocess
import pyperclip
from . import druid_encrypt_dialog as dialog
from utils import path


def show_message(message):
    """统一显示对话框

    根据传入对话框内容显示对话框

    参数:
        message (str): 对话框内容
    """
    wx.MessageDialog(None, message, '操作提醒', wx.OK).ShowModal()


class DruidEncrypt(dialog.DruidEncryptDialog):
    def __init__(self, parent):
        dialog.DruidEncryptDialog.__init__(self, parent)
        self.m_text_ctrl_original_password.SetHint('请输入原始密码（最长不超过32位）')
        self.Centre()

    def OnClickEventGenerate(self, event):
        """生成按钮事件"""
        # 校验原始密码是否输入
        original_password = self.m_text_ctrl_original_password.GetValue()
        if len(original_password) == 0:
            show_message('请输入原始密码')
            self.m_text_ctrl_original_password.SetFocus()
            return
        # 测试是否安装java环境
        subp = subprocess.Popen('java -version',
                                shell=True,
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT)
        out = subp.stdout.readlines()
        if (str(out[0]).find('java version') == -1):
            show_message('检查到您电脑上没有安装jdk，请安装jdk并且配置环境变量')
            return
        # 执行秘钥生成的命令
        jar_file = path.resource_path('libs/jar/druid-1.2.4.jar')
        original_password = self.m_text_ctrl_original_password.GetValue()
        cmd = 'java -cp ' + jar_file + \
            ' com.alibaba.druid.filter.config.ConfigTools ' + original_password
        subp = subprocess.Popen(cmd,
                                shell=True,
                                stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT,
                                encoding='gbk')
        out = subp.stdout.readlines()
        if (out[0].find('错误') > -1):
            show_message(','.join(out))
            return

        # 绑定值
        self.m_text_ctrl_private_key.SetValue(out[0].replace(
            '\n', '').split('privateKey:')[1])
        self.m_text_ctrl_public_key.SetValue(out[1].replace(
            '\n', '').split('publicKey:')[1])
        self.m_text_ctrl_encrypt_password.SetValue(out[2].replace(
            '\n', '').split('password:')[1])

    def OnClickEventPrivateKeyCopy(self, event):
        """复制私钥按钮事件"""
        text = self.m_text_ctrl_private_key.GetValue()
        if len(text) > 0:
            pyperclip.copy(text)
            show_message('私钥已复制到剪切板')
        else:
            show_message('请生成秘钥')

    def OnClickEventPublicKeyCopy(self, event):
        """复制公钥按钮事件"""
        text = self.m_text_ctrl_public_key.GetValue()
        if len(text) > 0:
            pyperclip.copy(text)
            show_message('公钥已复制到剪切板')
        else:
            show_message('请生成秘钥')

    def OnClickEventEncryptPasswordCopy(self, event):
        """复制密码按钮事件"""
        text = self.m_text_ctrl_encrypt_password.GetValue()
        if len(text) > 0:
            pyperclip.copy(text)
            show_message('密码已复制到剪切板')
        else:
            show_message('请生成密码')

    def OnClickEventClear(self, event):
        """清空按钮事件"""
        self.m_text_ctrl_original_password.SetValue('')
        self.m_text_ctrl_original_password.SetFocus()
        self.m_text_ctrl_private_key.SetValue('')
        self.m_text_ctrl_public_key.SetValue('')
        self.m_text_ctrl_encrypt_password.SetValue('')
