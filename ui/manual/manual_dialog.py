# -*- coding: utf-8 -*-

###########################################################################
# Python code generated with wxFormBuilder (version 3.10.1-0-g8feb16b)
# http://www.wxformbuilder.org/
##
# PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
# Class ManualDialog
###########################################################################


class ManualDialog (wx.Dialog):

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, id=wx.ID_ANY, title=u"使用指南", pos=wx.DefaultPosition, size=wx.Size(
            500, 459), style=wx.DEFAULT_DIALOG_STYLE)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        b_sizer_root = wx.BoxSizer(wx.VERTICAL)

        b_sizer_tab = wx.BoxSizer(wx.HORIZONTAL)

        self.m_notebook = wx.Notebook(
            self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_notebook_tab_1 = wx.ScrolledWindow(
            self.m_notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.HSCROLL | wx.VSCROLL)
        self.m_notebook_tab_1.SetScrollRate(5, 5)
        b_sizer_tab_1 = wx.BoxSizer(wx.VERTICAL)

        self.m_static_text_series_1 = wx.StaticText(
            self.m_notebook_tab_1, wx.ID_ANY, u"新版本已经支持官网版本和基于官网版本的扩展项目的修改\n\n注意：修改前请确认您要修改的项目是基于什么版本做的扩展，选择对应的\n系列\n", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_static_text_series_1.Wrap(-1)

        b_sizer_tab_1.Add(self.m_static_text_series_1,
                          0, wx.ALL | wx.EXPAND, 5)

        self.m_notebook_tab_1.SetSizer(b_sizer_tab_1)
        self.m_notebook_tab_1.Layout()
        b_sizer_tab_1.Fit(self.m_notebook_tab_1)
        self.m_notebook.AddPage(self.m_notebook_tab_1, u"系列说明", True)
        self.m_notebook_tab_2 = wx.ScrolledWindow(
            self.m_notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.VSCROLL)
        self.m_notebook_tab_2.SetScrollRate(5, 5)
        b_sizer_tab_2 = wx.BoxSizer(wx.VERTICAL)

        self.m_static_text_use_step = wx.StaticText(
            self.m_notebook_tab_2, wx.ID_ANY, u"使用步骤：", wx.Point(-1, -1), wx.Size(-1, -1), 0)
        self.m_static_text_use_step.Wrap(-1)

        b_sizer_tab_2.Add(self.m_static_text_use_step, 0, wx.ALL, 5)

        self.m_static_text_use_step_1 = wx.StaticText(
            self.m_notebook_tab_2, wx.ID_ANY, u"1、从官网下载ZIP格式的项目包，不同的版本前往不同的地址下载", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_static_text_use_step_1.Wrap(-1)

        b_sizer_tab_2.Add(self.m_static_text_use_step_1, 0, wx.ALL, 5)

        self.m_static_text_use_step_2 = wx.StaticText(
            self.m_notebook_tab_2, wx.ID_ANY, u"2、在修改器的主页选择项目的压缩包", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_static_text_use_step_2.Wrap(-1)

        b_sizer_tab_2.Add(self.m_static_text_use_step_2, 0, wx.ALL, 5)

        self.m_static_text_use_step_3 = wx.StaticText(
            self.m_notebook_tab_2, wx.ID_ANY, u"3、填写要修改的内容", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_static_text_use_step_3.Wrap(-1)

        b_sizer_tab_2.Add(self.m_static_text_use_step_3, 0, wx.ALL, 5)

        self.m_static_text_use_step_4 = wx.StaticText(
            self.m_notebook_tab_2, wx.ID_ANY, u"4、点击开始执行按钮进行修改", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_static_text_use_step_4.Wrap(-1)

        b_sizer_tab_2.Add(self.m_static_text_use_step_4, 0, wx.ALL, 5)

        self.m_static_text_use_step_5 = wx.StaticText(
            self.m_notebook_tab_2, wx.ID_ANY, u"5、修改完毕后点击打开输出目录按钮可查看已经修改好的项目", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_static_text_use_step_5.Wrap(-1)

        b_sizer_tab_2.Add(self.m_static_text_use_step_5, 0, wx.ALL, 5)

        self.m_notebook_tab_2.SetSizer(b_sizer_tab_2)
        self.m_notebook_tab_2.Layout()
        b_sizer_tab_2.Fit(self.m_notebook_tab_2)
        self.m_notebook.AddPage(self.m_notebook_tab_2, u"使用说明", False)
        self.m_notebook_tab_3 = wx.ScrolledWindow(
            self.m_notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.HSCROLL | wx.VSCROLL)
        self.m_notebook_tab_3.SetScrollRate(5, 5)
        b_sizer_tab_3 = wx.BoxSizer(wx.VERTICAL)

        sb_sizer_qa_1 = wx.StaticBoxSizer(wx.StaticBox(
            self.m_notebook_tab_3, wx.ID_ANY, u"修改后发现有的文件夹或者文件没有修改"), wx.VERTICAL)

        self.m_static_text_qa_1_a = wx.StaticText(sb_sizer_qa_1.GetStaticBox(
        ), wx.ID_ANY, u"解答：请确认修改的系列是否选择正确，如果正确，请检查是否修改了\n模板文件，具体可到“文件”→“模板配置（F7）”中查看和修改", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_static_text_qa_1_a.Wrap(-1)

        sb_sizer_qa_1.Add(self.m_static_text_qa_1_a, 0, wx.ALL, 5)

        b_sizer_tab_3.Add(sb_sizer_qa_1, 0, wx.ALL | wx.EXPAND, 5)

        sb_sizer_qa_2 = wx.StaticBoxSizer(wx.StaticBox(
            self.m_notebook_tab_3, wx.ID_ANY, u"修改器支持其他版本比如ruoyi-vue-activiti"), wx.VERTICAL)

        self.m_static_text_qa_2_a = wx.StaticText(sb_sizer_qa_2.GetStaticBox(
        ), wx.ID_ANY, u"解答：新版本已支持，选择好对应的系列就好，扩展项目中如果加了模\n块或者有特殊要改的文件，请自行编辑模板，参考上一个问题的答案", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_static_text_qa_2_a.Wrap(-1)

        sb_sizer_qa_2.Add(self.m_static_text_qa_2_a, 0, wx.ALL, 5)

        b_sizer_tab_3.Add(sb_sizer_qa_2, 0, wx.ALL | wx.EXPAND, 5)

        sb_sizer_qa_3 = wx.StaticBoxSizer(wx.StaticBox(
            self.m_notebook_tab_3, wx.ID_ANY, u"RuoYiApplication 类似的没有改"), wx.VERTICAL)

        self.m_static_text_qa_3_a = wx.StaticText(sb_sizer_qa_3.GetStaticBox(
        ), wx.ID_ANY, u"解答：是的，类似的没有修改，有需要可自己手动修改，另外作者、\njs相关命名都不做修改", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_static_text_qa_3_a.Wrap(-1)

        sb_sizer_qa_3.Add(self.m_static_text_qa_3_a, 0, wx.ALL, 5)

        b_sizer_tab_3.Add(sb_sizer_qa_3, 0, wx.ALL | wx.EXPAND, 5)

        sb_sizer_qa_4 = wx.StaticBoxSizer(wx.StaticBox(
            self.m_notebook_tab_3, wx.ID_ANY, u"微服务版本修改后发现启动不了"), wx.VERTICAL)

        self.m_static_text_qa_4_a = wx.StaticText(sb_sizer_qa_4.GetStaticBox(
        ), wx.ID_ANY, u"解答：请检查nacos中的配置是否正确", wx.DefaultPosition, wx.DefaultSize, 0)
        self.m_static_text_qa_4_a.Wrap(-1)

        sb_sizer_qa_4.Add(self.m_static_text_qa_4_a, 0, wx.ALL, 5)

        b_sizer_tab_3.Add(sb_sizer_qa_4, 0, wx.ALL | wx.EXPAND, 5)

        self.m_notebook_tab_3.SetSizer(b_sizer_tab_3)
        self.m_notebook_tab_3.Layout()
        b_sizer_tab_3.Fit(self.m_notebook_tab_3)
        self.m_notebook.AddPage(self.m_notebook_tab_3, u"常见问题", False)

        b_sizer_tab.Add(self.m_notebook, 1, wx.EXPAND | wx.ALL, 5)

        b_sizer_root.Add(b_sizer_tab, 1, wx.EXPAND, 5)

        b_sizer_button = wx.BoxSizer(wx.HORIZONTAL)

        self.m_button_close = wx.Button(
            self, wx.ID_CANCEL, u"关闭", wx.DefaultPosition, wx.DefaultSize, 0)
        b_sizer_button.Add(self.m_button_close, 0, wx.ALL, 5)

        b_sizer_root.Add(b_sizer_button, 0, wx.ALIGN_RIGHT, 5)

        self.SetSizer(b_sizer_root)
        self.Layout()

        self.Centre(wx.BOTH)

    def __del__(self):
        pass
