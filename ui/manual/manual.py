# -*- coding: utf-8 -*-
# @Time : 2021/07/06
# @Author : ricky
# @File : manual.py
# @Software: vscode
"""
使用指南
"""
import wx
from . import manual_dialog as dialog


class Manual(dialog.ManualDialog):
    def __init__(self, parent):
        dialog.ManualDialog.__init__(self, parent)
        self.Centre()
