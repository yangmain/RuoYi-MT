# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version 3.10.1-0-g8feb16b)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class MainFrame
###########################################################################

class MainFrame ( wx.Frame ):

	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"若依框架修改器", pos = wx.DefaultPosition, size = wx.Size( 850,550 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

		self.SetSizeHints( wx.Size( 850,550 ), wx.Size( -1,-1 ) )
		self.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )

		bSizer = wx.BoxSizer( wx.HORIZONTAL )

		bSizer23 = wx.BoxSizer( wx.VERTICAL )

		bSizer23.SetMinSize( wx.Size( 420,-1 ) )
		self.m_scrolled_window = wx.ScrolledWindow( self, wx.ID_ANY, wx.DefaultPosition, wx.Size( -1,-1 ), wx.VSCROLL )
		self.m_scrolled_window.SetScrollRate( 0, 5 )
		bSizer24 = wx.BoxSizer( wx.VERTICAL )

		sbSizer2 = wx.StaticBoxSizer( wx.StaticBox( self.m_scrolled_window, wx.ID_ANY, wx.EmptyString ), wx.VERTICAL )

		self.m_filePicker = wx.FilePickerCtrl( sbSizer2.GetStaticBox(), wx.ID_ANY, wx.EmptyString, u"请选择.zip文件", u"*.zip", wx.DefaultPosition, wx.Size( 200,-1 ), wx.FLP_DEFAULT_STYLE )
		self.m_filePicker.SetToolTip( u"请选择.zip文件" )

		sbSizer2.Add( self.m_filePicker, 0, wx.ALL|wx.EXPAND, 5 )

		m_radio_box_modeChoices = [ u"RuoYi标准版", u"RuoYi-Vue", u"RuoYi-fast", u"RuoYi-Cloud" ]
		self.m_radio_box_mode = wx.RadioBox( sbSizer2.GetStaticBox(), wx.ID_ANY, u"选择系列", wx.DefaultPosition, wx.Size( -1,-1 ), m_radio_box_modeChoices, 1, wx.RA_SPECIFY_ROWS )
		self.m_radio_box_mode.SetSelection( 0 )
		sbSizer2.Add( self.m_radio_box_mode, 0, wx.ALL|wx.EXPAND, 5 )

		sbSizer4 = wx.StaticBoxSizer( wx.StaticBox( sbSizer2.GetStaticBox(), wx.ID_ANY, u"修改内容" ), wx.VERTICAL )

		gSizer1 = wx.GridSizer( 6, 2, 0, 0 )

		self.m_static_text_project_dir_name = wx.StaticText( sbSizer4.GetStaticBox(), wx.ID_ANY, u"目录名称（示例：RuoYi）", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_static_text_project_dir_name.Wrap( -1 )

		gSizer1.Add( self.m_static_text_project_dir_name, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.m_text_ctrl_project_dir_name = wx.TextCtrl( sbSizer4.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer1.Add( self.m_text_ctrl_project_dir_name, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL|wx.EXPAND, 5 )

		self.m_static_text_project_name = wx.StaticText( sbSizer4.GetStaticBox(), wx.ID_ANY, u"项目名（示例：ruoyi）", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_static_text_project_name.Wrap( -1 )

		gSizer1.Add( self.m_static_text_project_name, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.m_text_ctrl_project_name = wx.TextCtrl( sbSizer4.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer1.Add( self.m_text_ctrl_project_name, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL|wx.EXPAND, 5 )

		self.m_static_text_package_name = wx.StaticText( sbSizer4.GetStaticBox(), wx.ID_ANY, u"包名（示例：com.ruoyi）", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_static_text_package_name.Wrap( -1 )

		gSizer1.Add( self.m_static_text_package_name, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.m_text_ctrl_package_name = wx.TextCtrl( sbSizer4.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_text_ctrl_package_name.SetToolTip( u"不限制级数" )

		gSizer1.Add( self.m_text_ctrl_package_name, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL|wx.EXPAND, 5 )

		self.m_static_text_artifact_id = wx.StaticText( sbSizer4.GetStaticBox(), wx.ID_ANY, u"artifactId（示例：ruoyi）", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_static_text_artifact_id.Wrap( -1 )

		gSizer1.Add( self.m_static_text_artifact_id, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.m_text_ctrl_artifact_id = wx.TextCtrl( sbSizer4.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer1.Add( self.m_text_ctrl_artifact_id, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL|wx.EXPAND, 5 )

		self.m_static_text_group_id = wx.StaticText( sbSizer4.GetStaticBox(), wx.ID_ANY, u"groupId（示例：com.ruoyi）", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_static_text_group_id.Wrap( -1 )

		gSizer1.Add( self.m_static_text_group_id, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.m_text_ctrl_group_id = wx.TextCtrl( sbSizer4.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_text_ctrl_group_id.SetToolTip( u"不限制级数" )

		gSizer1.Add( self.m_text_ctrl_group_id, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL|wx.EXPAND, 5 )

		self.m_static_text_site_name = wx.StaticText( sbSizer4.GetStaticBox(), wx.ID_ANY, u"站点名称（示例：若依管理系统）", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_static_text_site_name.Wrap( -1 )

		gSizer1.Add( self.m_static_text_site_name, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.m_text_ctrl_site_name = wx.TextCtrl( sbSizer4.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer1.Add( self.m_text_ctrl_site_name, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL|wx.EXPAND, 5 )


		sbSizer4.Add( gSizer1, 1, wx.EXPAND, 5 )


		sbSizer2.Add( sbSizer4, 0, wx.ALL|wx.EXPAND, 5 )

		bSizer6 = wx.BoxSizer( wx.VERTICAL )

		bSizer3 = wx.BoxSizer( wx.HORIZONTAL )

		self.m_btn_start = wx.Button( sbSizer2.GetStaticBox(), wx.ID_ANY, u"开始执行", wx.DefaultPosition, wx.Size( -1,-1 ), 0 )
		bSizer3.Add( self.m_btn_start, 0, wx.ALL, 5 )

		self.m_btn_clear = wx.Button( sbSizer2.GetStaticBox(), wx.ID_ANY, u"清空", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer3.Add( self.m_btn_clear, 0, wx.ALL, 5 )

		self.m_btn_open_output_directory = wx.Button( sbSizer2.GetStaticBox(), wx.ID_ANY, u"打开输出目录", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_btn_open_output_directory.Enable( False )

		bSizer3.Add( self.m_btn_open_output_directory, 0, wx.ALL, 5 )

		self.m_btn_reward = wx.Button( sbSizer2.GetStaticBox(), wx.ID_ANY, u"朕要赏(F8)", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer3.Add( self.m_btn_reward, 0, wx.ALL, 5 )


		bSizer6.Add( bSizer3, 1, wx.EXPAND, 5 )

		self.m_static_text_tip1 = wx.StaticText( sbSizer2.GetStaticBox(), wx.ID_ANY, u"注：需要修改其他参数可以打开\"文件\">\"配置参数\"(F6)进行设置", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_static_text_tip1.Wrap( -1 )

		bSizer6.Add( self.m_static_text_tip1, 0, wx.ALL, 5 )


		sbSizer2.Add( bSizer6, 0, wx.EXPAND, 5 )


		bSizer24.Add( sbSizer2, 1, wx.ALL|wx.EXPAND, 5 )


		self.m_scrolled_window.SetSizer( bSizer24 )
		self.m_scrolled_window.Layout()
		bSizer24.Fit( self.m_scrolled_window )
		bSizer23.Add( self.m_scrolled_window, 1, wx.EXPAND, 5 )


		bSizer.Add( bSizer23, 0, wx.EXPAND, 5 )

		sbSizer3 = wx.StaticBoxSizer( wx.StaticBox( self, wx.ID_ANY, u"操作记录" ), wx.VERTICAL )

		self.m_text_log = wx.TextCtrl( sbSizer3.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE|wx.TE_READONLY )
		sbSizer3.Add( self.m_text_log, 1, wx.ALL|wx.EXPAND, 5 )


		bSizer.Add( sbSizer3, 1, wx.ALL|wx.EXPAND, 5 )


		self.SetSizer( bSizer )
		self.Layout()
		self.m_menubar = wx.MenuBar( 0 )
		self.m_file = wx.Menu()
		self.m_menu_item_config = wx.MenuItem( self.m_file, wx.ID_ANY, u"配置参数"+ u"\t" + u"F6", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_file.Append( self.m_menu_item_config )

		self.m_menu_item_template_config = wx.MenuItem( self.m_file, wx.ID_ANY, u"模板配置"+ u"\t" + u"F7", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_file.Append( self.m_menu_item_template_config )

		self.m_menu_item_exit = wx.MenuItem( self.m_file, wx.ID_ANY, u"退出程序"+ u"\t" + u"Alt+F4", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_file.Append( self.m_menu_item_exit )

		self.m_menubar.Append( self.m_file, u"文件" )

		self.m_menu_tools = wx.Menu()
		self.m_menu_item_druid_encrypt = wx.MenuItem( self.m_menu_tools, wx.ID_ANY, u"druid秘钥生成", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu_tools.Append( self.m_menu_item_druid_encrypt )

		self.m_menu_item_deletefile = wx.MenuItem( self.m_menu_tools, wx.ID_ANY, u"批量删除文件", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu_tools.Append( self.m_menu_item_deletefile )

		self.m_menubar.Append( self.m_menu_tools, u"工具" )

		self.m_menu_help = wx.Menu()
		self.m_menu_item_reward = wx.MenuItem( self.m_menu_help, wx.ID_ANY, u"打赏作者"+ u"\t" + u"F8", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu_help.Append( self.m_menu_item_reward )

		self.m_menu_item_about = wx.MenuItem( self.m_menu_help, wx.ID_ANY, u"关于我们"+ u"\t" + u"F1", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu_help.Append( self.m_menu_item_about )

		self.m_menu_item_upgrade = wx.MenuItem( self.m_menu_help, wx.ID_ANY, u"检测更新", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu_help.Append( self.m_menu_item_upgrade )

		self.m_menu_item_manual = wx.MenuItem( self.m_menu_help, wx.ID_ANY, u"使用指南", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu_help.Append( self.m_menu_item_manual )

		self.m_menu_item_app_dir = wx.MenuItem( self.m_menu_help, wx.ID_ANY, u"程序目录", wx.EmptyString, wx.ITEM_NORMAL )
		self.m_menu_help.Append( self.m_menu_item_app_dir )

		self.m_menubar.Append( self.m_menu_help, u"帮助" )

		self.SetMenuBar( self.m_menubar )

		self.m_statusbar = self.CreateStatusBar( 1, wx.STB_SIZEGRIP, wx.ID_ANY )

		self.Centre( wx.HORIZONTAL )

		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.OnMenuClickEventExit )
		self.m_btn_start.Bind( wx.EVT_BUTTON, self.OnClickEventStart )
		self.m_btn_clear.Bind( wx.EVT_BUTTON, self.OnClickEventClear )
		self.m_btn_open_output_directory.Bind( wx.EVT_BUTTON, self.OnClickEventOpenOutputDirectory )
		self.m_btn_reward.Bind( wx.EVT_BUTTON, self.OnMenuClickEventReward )
		self.Bind( wx.EVT_MENU, self.OnMenuClickEventConfig, id = self.m_menu_item_config.GetId() )
		self.Bind( wx.EVT_MENU, self.OnMenuClickEventTemplateConfig, id = self.m_menu_item_template_config.GetId() )
		self.Bind( wx.EVT_MENU, self.OnMenuClickEventExit, id = self.m_menu_item_exit.GetId() )
		self.Bind( wx.EVT_MENU, self.OnMenuClickEventDruidEncrypt, id = self.m_menu_item_druid_encrypt.GetId() )
		self.Bind( wx.EVT_MENU, self.OnMenuClickEventDeleteFile, id = self.m_menu_item_deletefile.GetId() )
		self.Bind( wx.EVT_MENU, self.OnMenuClickEventReward, id = self.m_menu_item_reward.GetId() )
		self.Bind( wx.EVT_MENU, self.OnAbout, id = self.m_menu_item_about.GetId() )
		self.Bind( wx.EVT_MENU, self.OnUpgrade, id = self.m_menu_item_upgrade.GetId() )
		self.Bind( wx.EVT_MENU, self.OnManual, id = self.m_menu_item_manual.GetId() )
		self.Bind( wx.EVT_MENU, self.OnOpenAppDir, id = self.m_menu_item_app_dir.GetId() )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def OnMenuClickEventExit( self, event ):
		event.Skip()

	def OnClickEventStart( self, event ):
		event.Skip()

	def OnClickEventClear( self, event ):
		event.Skip()

	def OnClickEventOpenOutputDirectory( self, event ):
		event.Skip()

	def OnMenuClickEventReward( self, event ):
		event.Skip()

	def OnMenuClickEventConfig( self, event ):
		event.Skip()

	def OnMenuClickEventTemplateConfig( self, event ):
		event.Skip()


	def OnMenuClickEventDruidEncrypt( self, event ):
		event.Skip()

	def OnMenuClickEventDeleteFile( self, event ):
		event.Skip()


	def OnAbout( self, event ):
		event.Skip()

	def OnUpgrade( self, event ):
		event.Skip()

	def OnManual( self, event ):
		event.Skip()

	def OnOpenAppDir( self, event ):
		event.Skip()


