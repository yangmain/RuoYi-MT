# -*- coding: utf-8 -*-
# @Time : 2021/02/23
# @Author : ricky
# @File : about.py
# @Software: vscode
"""
关于我们界面
"""
from constant import constant
from . import about_dialog as dialog


class About(dialog.AboutDialog):
    def __init__(self, parent):
        dialog.AboutDialog.__init__(self, parent)
        self.m_static_text_version.SetLabelText('当前版本号：' +
                                                constant.APP_VERSION_STRING)
        self.Centre()
