# -*- coding: utf-8 -*-
# @Time : 2021/02/20
# @Author : ricky
# @File : config.py
# @Software: vscode
"""
参数配置界面
"""
import wx
from . import config_dialog as dialog
from utils import configini


class Config(dialog.ConfigDialog):
    """
    主类
    """

    def __init__(self, parent):
        dialog.ConfigDialog.__init__(self, parent)
        self.configini = configini.Config()
        config_enable = self.configini.get_value('config', 'enable')
        database_ip = self.configini.get_value('database', 'ip')
        database_port = self.configini.get_value('database', 'port')
        database_name = self.configini.get_value('database', 'name')
        database_username = self.configini.get_value('database', 'username')
        database_password = self.configini.get_value('database', 'password')

        redis_ip = self.configini.get_value('redis', 'ip')
        redis_port = self.configini.get_value('redis', 'port')
        redis_password = self.configini.get_value('redis', 'password')

        self.m_property_item_enable.SetValue(config_enable)

        self.m_property_item_database_ip.SetValue(database_ip)
        if len(database_port) > 0:
            self.m_property_item_database_port.SetValue(int(database_port))
        self.m_property_item_database_name.SetValue(database_name)
        self.m_property_item_database_username.SetValue(database_username)
        self.m_property_item_database_password.SetValue(database_password)

        self.m_property_item_redis_ip.SetValue(redis_ip)
        if len(redis_port) > 0:
            self.m_property_item_redis_port.SetValue(int(redis_port))
        self.m_property_item_redis_password.SetValue(redis_password)
        self.Centre()

    def OnClickEventSave(self, event):
        values = self.m_property_page.GetPropertyValues()
        for key in values:
            key_array = key.split('_', 1)
            value = str(values[key])
            self.configini.set_value(key_array[0], key_array[1], value)
        self.configini.write()
        self.EndModal(wx.ID_OK)
