# -*- coding: utf-8 -*-

###########################################################################
## Python code generated with wxFormBuilder (version 3.10.1-0-g8feb16b3)
## http://www.wxformbuilder.org/
##
## PLEASE DO *NOT* EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class TemplateConfigDialog
###########################################################################

class TemplateConfigDialog ( wx.Dialog ):

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"模板配置", pos = wx.DefaultPosition, size = wx.Size( 600,460 ), style = wx.DEFAULT_DIALOG_STYLE|wx.MAXIMIZE_BOX|wx.RESIZE_BORDER )

		self.SetSizeHints( wx.DefaultSize, wx.DefaultSize )

		bSizer = wx.BoxSizer( wx.VERTICAL )

		self.m_textCtrl = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_MULTILINE )
		bSizer.Add( self.m_textCtrl, 1, wx.ALL|wx.EXPAND, 5 )

		bSizer_bottom = wx.BoxSizer( wx.HORIZONTAL )

		self.m_staticText = wx.StaticText( self, wx.ID_ANY, u"*对Windows配置文件(.ini)不熟悉的请勿修改此文件", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText.Wrap( -1 )

		self.m_staticText.SetForegroundColour( wx.Colour( 255, 0, 0 ) )

		bSizer_bottom.Add( self.m_staticText, 1, wx.ALIGN_CENTER|wx.ALL, 5 )

		self.m_button_restore_default = wx.Button( self, wx.ID_ANY, u"恢复默认配置", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer_bottom.Add( self.m_button_restore_default, 0, wx.ALL, 5 )

		self.m_button_cancel = wx.Button( self, wx.ID_CANCEL, u"取消", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer_bottom.Add( self.m_button_cancel, 0, wx.ALL, 5 )

		self.m_button_save = wx.Button( self, wx.ID_ANY, u"保存", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer_bottom.Add( self.m_button_save, 0, wx.ALL, 5 )


		bSizer.Add( bSizer_bottom, 0, wx.EXPAND, 5 )


		self.SetSizer( bSizer )
		self.Layout()

		self.Centre( wx.BOTH )

		# Connect Events
		self.m_button_restore_default.Bind( wx.EVT_BUTTON, self.OnClickEventRestoreDefault )
		self.m_button_save.Bind( wx.EVT_BUTTON, self.OnClickEventSave )

	def __del__( self ):
		pass


	# Virtual event handlers, override them in your derived class
	def OnClickEventRestoreDefault( self, event ):
		event.Skip()

	def OnClickEventSave( self, event ):
		event.Skip()


