# -*- coding: utf-8 -*-
# @Time : 2022/05/05
# @Author : ricky
# @File : templateconfig.py
# @Software: vscode
"""
模板配置
"""
import wx
import os
from shutil import copy
from utils import path
from . import template_config_dialog as dialog


class TemplateConfig(dialog.TemplateConfigDialog):
    def __init__(self, parent):
        dialog.TemplateConfigDialog.__init__(self, parent)
        # 读取模板配置文件
        self.tpath = path.resource_path('template_config.ini')
        self.deftpath = path.resource_path(
            os.path.join('conf', 'template_config.ini'))
        if not os.path.exists(self.tpath):
            # 如果文件不存在，复制默认的模板配置文件到当前目录
            copy(self.deftpath, self.tpath)
        self.read_content_to_input()
        self.Centre()

    def read_content_to_input(self, is_restore=False):
        """
        读取文件内容到输入框

        参数:
            is_restore (bool): 是否恢复默认配置
        """
        if is_restore:
            path = self.deftpath
        else:
            path = self.tpath
        with open(path, 'r', encoding="utf-8") as file:
            content = file.read()
            self.m_textCtrl.SetValue(content)

    # 恢复默认配置按钮
    def OnClickEventRestoreDefault(self, event):
        dlg = wx.MessageDialog(
            None, '请确认是否要恢复默认配置，当前修改将不会保留', '恢复提醒', wx.YES_NO)
        if dlg.ShowModal() == wx.ID_YES:
            self.read_content_to_input(True)
        dlg.Destroy()

    # 保存按钮
    def OnClickEventSave(self, event):
        self.EndModal(wx.ID_OK)
        content = self.m_textCtrl.GetValue()
        with open(self.tpath, 'w', encoding="utf-8") as file:
            file.write(content)
