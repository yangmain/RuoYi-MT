# -*- coding: utf-8 -*-
# @Time : 2021/02/20
# @Author : ricky
# @File : path.py
# @Software: vscode
"""
路径工具类
"""
import os
import sys


def resource_path(relative_path):
    """
    返回资源绝对路径
    
    参数:
        relative_path (str): 相对路径或者资源名称
    返回:
        绝对路径（带临时目录的）
    """
    if hasattr(sys, '_MEIPASS'):
        # PyInstaller会创建临时文件夹temp
        # 并把路径存储在_MEIPASS中
        exepath = sys._MEIPASS
    else:
        exepath = os.path.dirname(os.path.realpath(sys.argv[0]))
    return os.path.join(exepath, relative_path)